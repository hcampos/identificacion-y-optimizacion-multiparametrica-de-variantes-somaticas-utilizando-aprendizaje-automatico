
# coding: utf-8

# In[2]:


from glob import glob
from vcf import Reader as vcf_reader
from numpy import polyfit
import pandas as pd


# In[3]:


data = []
for f in glob('/home/hcampos/common_bioinformatics/files/vcf_somsig/vcf_somsig_dataset_totally/sp005fijo/DT_uy_4_TU*.vcf'):
    print (f)
    c = int(f.split('_')[-3][2:])
    v = float("0.{}".format(f.split('_')[-2][4:]))
    t = float("0.{}".format(f.split('.')[0].split('_')[-1][3:]))
    with open(f) as vcf:
        for r in vcf_reader(vcf):
            data.append([c, v, t, r.REF, str(r.ALT[0])])
df_mvf = pd.DataFrame(data, columns=['min_cov','min_var_freq', 'somatic_p', 'ref', 'alt'])


# In[4]:


df_mvf = df_mvf[df_mvf.ref != 'N']


# In[334]:


df_mvf_by_v = pd.DataFrame(df_mvf.groupby(['min_cov','min_var_freq','somatic_p']).size()).rename(columns={0: 'n'}).reset_index()


# In[336]:


export_csv = df_mvf_by_v.to_csv (r'/home/hcampos/common_bioinformatics/files/csv_dataframe/dataset/1fij2var/sp005fijo/export_dataframe_DT_uy_4_TU.csv', index = None, header=True) #Don't forget to add '.csv' at the end of the path
#export_csv = df_mvf_by_v.to_csv (r'/home/hcampos/common_bioinformatics/files/csv_dataframe/xTrain/export_dataframe_DT_uy_0_TU.csv', index = None, header=True) #Don't forget to add '.csv' at the end of the path


# In[337]:


export_csv

