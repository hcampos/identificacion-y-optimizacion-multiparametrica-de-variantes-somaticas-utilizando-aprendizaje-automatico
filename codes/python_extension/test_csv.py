
# coding: utf-8

# In[191]:


from glob import glob
from vcf import Reader as vcf_reader
from numpy import polyfit
import pandas as pd


# In[217]:


data = []
for f in glob('/home/hcampos/common_bioinformatics/files/vcf_somsig/vcf_somsig_test/DT_uy_4_TU*.vcf'):
    #print (f)
    c = int(f.split('_')[-3][2:])
    v = float("0.{}".format(f.split('_')[-2][4:]))
    t = float("0.{}".format(f.split('.')[0].split('_')[-1][3:]))
    with open(f) as vcf:
        for r in vcf_reader(vcf):
            data.append([c, v, t, r.REF, str(r.ALT[0])])
df_mvf = pd.DataFrame(data, columns=['min_cov','min_var_freq', 'somatic_p', 'ref', 'alt'])


# In[218]:


df_mvf = df_mvf[df_mvf.ref != 'N']


# In[220]:


df_mvf_by_v = pd.DataFrame(df_mvf.groupby(['min_cov','min_var_freq','somatic_p']).size()).rename(columns={0: 'n'}).reset_index()


# In[222]:


export_csv = df_mvf_by_v.to_csv (r'/home/hcampos/common_bioinformatics/files/csv_dataframe/xTest/export_dataframe_DT_uy_4_TU.csv', index = None, header=True) #Don't forget to add '.csv' at the end of the path


# In[223]:


export_csv

