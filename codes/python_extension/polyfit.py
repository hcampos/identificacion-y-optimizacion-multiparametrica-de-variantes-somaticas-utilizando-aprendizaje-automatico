
# coding: utf-8

# In[1]:


import numpy as np
import pandas as pd 
import seaborn as sns
from sklearn import linear_model
from sklearn import metrics
from sklearn.model_selection import train_test_split 
from sklearn.metrics import r2_score
from sklearn.linear_model import LinearRegression

from sklearn.preprocessing import PolynomialFeatures
import matplotlib.pyplot as plt
import numpy.polynomial as pol
from mpl_toolkits.mplot3d import Axes3D
get_ipython().run_line_magic('matplotlib', 'inline')


# In[345]:


data_train = pd.read_csv('/home/hcampos/common_bioinformatics/files/csv_dataframe/xTrain/export_dataframe_TCL1_usa_117_NO.csv', index_col=False, header=0)
data_test = pd.read_csv('/home/hcampos/common_bioinformatics/files/csv_dataframe/xTest/export_dataframe_TCL1_usa_117_NO.csv', index_col=False, header=0)

x_train = data_train[['min_cov', 'min_var_freq','somatic_p']].values
y_train = data_train['n'].values

x_test = data_test[['min_cov', 'min_var_freq','somatic_p']].values
y_test = data_test['n'].values


# In[346]:


poly = PolynomialFeatures(degree=3)


# In[347]:


x_ = poly.fit_transform(x_train)


# In[348]:


clf = linear_model.LinearRegression(fit_intercept=False)
clf.fit(x_, y_train)


# In[349]:


#poly.powers_


# In[337]:


df_coeff = pd.DataFrame(clf.coef_)


# In[356]:


clf.coef_


# In[341]:


pred_ = poly.fit_transform(x_test)


# In[342]:


y_pred = clf.predict(pred_)


# In[343]:


df = pd.DataFrame({'Test': y_test, 'Model': y_pred})


# In[350]:


df


# In[18]:


df.plot(kind='bar',figsize=(10,8))
plt.grid(which='major', linestyle='-', linewidth='0.5', color='green')
plt.grid(which='minor', linestyle=':', linewidth='0.5', color='black')
plt.show()


# In[19]:


print('Mean Absolute Error:', metrics.mean_absolute_error(y_test, y_pred))  
print('Mean Squared Error:', metrics.mean_squared_error(y_test, y_pred))  
print('Root Mean Squared Error:', np.sqrt(metrics.mean_squared_error(y_test, y_pred)))


# In[20]:


df_uy = pd.read_csv("/home/hcampos/common_bioinformatics/files/csv_dataframe/boxplot_error/samples_box_uy.csv")   
df_uy["point-error"] = abs(df_uy.actual-df_uy.predict)


# In[21]:


fig = plt.subplots(figsize=(30, 8))
sns.boxplot(data=df_uy, x = "samples", y="point-error")
sns.despine()


# In[14]:


df_usa = pd.read_csv("/home/hcampos/common_bioinformatics/files/csv_dataframe/boxplot_error/samples_box_usa.csv")   
df_usa["point-error"] = abs(df_usa.actual-df_usa.predict)


# In[15]:


fig = plt.subplots(figsize=(30, 8))
sns.boxplot(data=df_usa, x = "samples", y="point-error")
sns.despine()

