
# coding: utf-8

# In[289]:


from glob import glob
from vcf import Reader as vcf_reader
from numpy import polyfit
import pandas as pd
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
import time
import os


# # Min Var Freq

# In[290]:


data = []
for f in glob('/home/hcampos/common_bioinformatics/files/vcf_somsig/mvf/DT_uy_4_TU*.vcf'):
    print (f)
    v = float("0.{}".format(f.split('_')[-2][4:]))
    with open(f) as vcf:
        for r in vcf_reader(vcf):
            data.append([v, r.REF, str(r.ALT[0])])
df_mvf = pd.DataFrame(data, columns=['min_var_freq', 'ref', 'alt'])


# In[291]:


df_mvf = df_mvf[df_mvf.ref != 'N']


# In[292]:


df_mvf_by_v = pd.DataFrame(df_mvf.groupby('min_var_freq').size()).rename(columns={0: 'n'}).reset_index()
sns.pointplot(data=df_mvf_by_v, x='min_var_freq', y='n')
sns.set(style="whitegrid")

i = 1
plot = sns.pointplot(data=df_mvf_by_v, x='min_var_freq', y='n')
fig = plot.get_figure()
while os.path.exists('{}{:d}.pdf'.format('/home/hcampos/thesis/memory/memory_used/figures/plot/variant_analysis/mvf/output', i)):
    i += 1
fig.savefig('{}{:d}.pdf'.format('/home/hcampos/thesis/memory/memory_used/figures/plot/variant_analysis/mvf/output', i))


# In[293]:


df_mvf_by_v


# In[294]:


df_mvf_by_mut = pd.DataFrame(df_mvf.groupby(['min_var_freq', 'ref', 'alt']).size()).rename(columns={0: 'n'}).reset_index()
df_mvf_by_mut['perc'] = df_mvf_by_mut.n / df_mvf_by_mut.n.sum()

#savefigure
i = 1
fig = sns.catplot(data=df_mvf_by_mut, x='min_var_freq', y='n', hue='alt', col='ref', kind='point')
#fig = plot.savefig()
while os.path.exists('{}{:d}.pdf'.format('/home/hcampos/thesis/memory/memory_used/figures/plot/variant_analysis/mvf/output', i)):
    i += 1
fig.savefig('{}{:d}.pdf'.format('/home/hcampos/thesis/memory/memory_used/figures/plot/variant_analysis/mvf/output', i))


# # Somatic P

# In[295]:


data = []
for f in glob('/home/hcampos/common_bioinformatics/files/vcf_somsig/sp/DT_uy_4_TU*.vcf'):
    print (f)
    v = float("0.{}".format(f.split('.')[0].split('_')[-1][3:]))
    with open(f) as vcf:
        for r in vcf_reader(vcf):
            data.append([v, r.REF, str(r.ALT[0])])
df_sp = pd.DataFrame(data, columns=['somatic_p', 'ref', 'alt'])


# In[296]:


df_sp = df_sp[df_sp.ref != 'N']


# In[297]:


df_sp_by_v = pd.DataFrame(df_sp.groupby('somatic_p').size()).rename(columns={0: 'n'}).reset_index()
sns.pointplot(data=df_sp_by_v, x='somatic_p', y='n')

i = 1
plot = sns.pointplot(data=df_sp_by_v, x='somatic_p', y='n')
fig = plot.get_figure()
while os.path.exists('{}{:d}.pdf'.format('/home/hcampos/thesis/memory/memory_used/figures/plot/variant_analysis/sp/output', i)):
    i += 1
fig.savefig('{}{:d}.pdf'.format('/home/hcampos/thesis/memory/memory_used/figures/plot/variant_analysis/sp/output', i))


# In[298]:


df_sp_by_v


# In[299]:


df_sp_by_mut = pd.DataFrame(df_sp.groupby(['somatic_p', 'ref', 'alt']).size()).rename(columns={0: 'n'}).reset_index()
df_sp_by_mut['perc'] = df_sp_by_mut.n / df_sp_by_mut.n.sum()

i = 1
fig = sns.catplot(data=df_sp_by_mut, x='somatic_p', y='n', hue='alt', col='ref', kind='point')

while os.path.exists('{}{:d}.pdf'.format('/home/hcampos/thesis/memory/memory_used/figures/plot/variant_analysis/sp/output', i)):
    i += 1
fig.savefig('{}{:d}.pdf'.format('/home/hcampos/thesis/memory/memory_used/figures/plot/variant_analysis/sp/output', i))


# # Minimun Coverage

# In[300]:


data = []
for f in glob('/home/hcampos/common_bioinformatics/files/vcf_somsig/mc/DT_uy_4_TU*.vcf'):
    print (f)
    v = int(f.split('_')[-3][2:])
    with open(f) as vcf:
        for r in vcf_reader(vcf):
            data.append([v, r.REF, str(r.ALT[0])])
df_mc = pd.DataFrame(data, columns=['min_cov', 'ref', 'alt'])


# In[301]:


df_mc = df_mc[df_mc.ref != 'N']


# In[302]:


df_mc_by_v = pd.DataFrame(df_mc.groupby('min_cov').size()).rename(columns={0: 'n'}).reset_index()
sns.pointplot(data=df_mc_by_v, x='min_cov', y='n')

i = 1
plot = sns.pointplot(data=df_mc_by_v, x='min_cov', y='n')
fig = plot.get_figure()
while os.path.exists('{}{:d}.pdf'.format('/home/hcampos/thesis/memory/memory_used/figures/plot/variant_analysis/mc/output', i)):
    i += 1
fig.savefig('{}{:d}.pdf'.format('/home/hcampos/thesis/memory/memory_used/figures/plot/variant_analysis/mc/output', i))


# In[303]:


df_mc_by_v


# In[304]:


df_mc_by_mut = pd.DataFrame(df_mc.groupby(['min_cov', 'ref', 'alt']).size()).rename(columns={0: 'n'}).reset_index()
df_mc_by_mut['perc'] = df_mc_by_mut.n / df_mc_by_mut.n.sum()

i = 1
fig = sns.catplot(data=df_mc_by_mut, x='min_cov', y='n', hue='alt', col='ref', kind='point')
while os.path.exists('{}{:d}.pdf'.format('/home/hcampos/thesis/memory/memory_used/figures/plot/variant_analysis/mc/output', i)):
    i += 1
fig.savefig('{}{:d}.pdf'.format('/home/hcampos/thesis/memory/memory_used/figures/plot/variant_analysis/mc/output', i))

