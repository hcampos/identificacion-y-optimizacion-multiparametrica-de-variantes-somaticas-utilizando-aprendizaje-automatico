
# coding: utf-8

# In[11]:


import numpy
import scipy.optimize as optimize
from scipy.optimize import basinhopping


# In[13]:


coef1 = clf.coef_[0]
coef2 = clf.coef_[1]
coef3 = clf.coef_[2]
coef4 = clf.coef_[3]
coef5 = clf.coef_[4]
coef6 = clf.coef_[5]
coef7 = clf.coef_[6]
coef8 = clf.coef_[7]
coef9 = clf.coef_[8]
coef10 = clf.coef_[9]
coef11 = clf.coef_[10]
coef12 = clf.coef_[11]
coef13 = clf.coef_[12]
coef14 = clf.coef_[13]
coef15 = clf.coef_[14]
coef16 = clf.coef_[15]
coef17 = clf.coef_[16]
coef18 = clf.coef_[17]
coef19 = clf.coef_[18]
coef20 = clf.coef_[19]


# In[5]:


def Obj_func(fun, sign= -1.0):      
    mc,mvf,sp = fun
    f = sign*(coef1 + coef2*mc + coef3*mvf + coef4*sp + coef5*mc**2 +  coef6*mc*mvf + coef7*mc*sp + coef8*mvf**2 + coef9*mvf*sp +  coef10*sp**2 +  coef11*mc**3 + coef12*mc**2*mvf + coef13*mc**2*sp + coef14*mc*mvf**2 + coef15*mc*mvf*sp + coef16*mc*sp**2 + coef17*mvf**3 + coef18*mvf**2*sp + coef19*mvf*sp**2 + coef20*sp**3)
    return f


# In[10]:


initial_guess = [4,0.01,0.01]

bnds = ((4, 13), (0.01,0.20),(0.01,0.05)) 
  
minimizer_kwargs = dict(method="SLSQP", bounds=bnds)
res = basinhopping(Obj_func, initial_guess, minimizer_kwargs=minimizer_kwargs)
#res=optimize.minimize(Obj_func, initial_guess,  method="SLSQP", bounds=bnds)

print ("Result",res)

